FLANN - Fast Library for Approximate Nearest Neighbors
======================================================

FLANN is a library for performing fast approximate nearest neighbor searches in high dimensional spaces. It contains a collection of algorithms we found to work best for nearest neighbor search and a system for automatically choosing the best algorithm and optimum parameters depending on the dataset.
FLANN is written in C++ and contains bindings for the following languages: C, MATLAB and Python.


Documentation
-------------

Check FLANN web page [here](http://www.cs.ubc.ca/~mariusm/flann).

Documentation on how to use the library can be found in the doc/manual.pdf file included in the release archives.

More information and experimental results can be found in the following paper:

  * Marius Muja and David G. Lowe, "Fast Approximate Nearest Neighbors with Automatic Algorithm Configuration", in International Conference on Computer Vision Theory and Applications (VISAPP'09), 2009 [(PDF)](http://people.cs.ubc.ca/~mariusm/uploads/FLANN/flann_visapp09.pdf) [(BibTex)](http://people.cs.ubc.ca/~mariusm/index.php/FLANN/BibTex)


Getting FLANN
-------------

The latest version of FLANN can be downloaded from here:

 *  Version 1.8.4 (15 January 2013)
    [flann-1.8.4-src.zip](http://people.cs.ubc.ca/~mariusm/uploads/FLANN/flann-1.8.4-src.zip) (Source code)  
    [User manual](http://people.cs.ubc.ca/~mariusm/uploads/FLANN/flann_manual-1.8.4.pdf)  
    [Changelog](https://github.com/mariusmuja/flann/blob/master/ChangeLog)  

If you want to try out the latest changes or contribute to FLANN, then it's recommended that you checkout the git source repository: `git clone git://github.com/mariusmuja/flann.git`

If you just want to browse the repository, you can do so by going [here](https://github.com/mariusmuja/flann).


Conditions of use
-----------------

FLANN is distributed under the terms of the [BSD License](https://github.com/mariusmuja/flann/blob/master/COPYING).

Bug reporting
-------------

Please report bugs or feature requests using [github's issue tracker](http://github.com/mariusmuja/flann/issues).

Note on CUDA
-------------
NVidia changed the default behavior with CUDA 5. The problem is, that the methods in result_set.h are used on device as well as on host. You can fix this with declaring all methods in result_set.h from __device__ to __device__ __host__. Then the methods will be compiled twice, once for device usage, once for host usage.
For example:

\_\_device\_\_
SingleResultSet( DistanceType eps ) : bestIndex(-1),bestDist(INFINITY), epsError(eps){ }
// change to:
\_\_device\_\_ \_\_host\_\_
SingleResultSet( DistanceType eps ) : bestIndex(-1),bestDist(INFINITY), epsError(eps){ }
Additionally you need to change the infinity() method because __int_as_float is device only:

\_\_device\_\_ \_\_host\_\_ \_\_forceinline\_\_
float infinity()
{
     //return __int_as_float(0x7f800000);
    return 2139095040.f;
     // This seems either way to be a better solution, because infinity is a constant
}